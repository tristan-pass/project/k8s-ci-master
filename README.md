敏捷开发的运作流程:

|        产品        |              测试              |           开发            |   运维   |
| :----------------: | :----------------------------: | :-----------------------: | :------: |
|      分析任务      |                                |                           |          |
| 产出“数据流动”文档 |         分配并追踪任务         | 成员自行领取任务/分配任务 |          |
|                    |                                | 分析任务并分配任务给运维  | 分析任务 |
|                    |       构建工程并测试任务       |     完成任务/交付测试     | 完成任务 |
|                    | 测试不通过则驳回给开发重新完成 |                           |          |
|                    |  测试通过则更新生产环境的版本  |                           |          |







这样之后k8s的ci/cd怎么做?

​	测试人员更新deployment的版本



开发/测试/生产 环境怎么推动?

​	如果从时间来看,"开发构建之后去更新deployment的版本" 与 "本地构建后上传到服务器再关闭原来服务再启动原来服务再看日志" 花费的时间并不相差,容错率以及占用时间前者更加适合敏捷开发





# 在宿主机安装kubectl

```
# 配置镜像仓库地址
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

# 安装kubectl
yum install -y kubectl
```



下载 ~/.kube/config

```
mkdir -p ~/.kube
vi ~/.kube/config

apiVersion: v1
kind: Config
clusters:
- cluster:
    api-version: v1
    insecure-skip-tls-verify: true
    server: "https://192.168.71.223:8765/r/projects/1a5/kubernetes:6443"
  name: "Default"
contexts:
- context:
    cluster: "Default"
    user: "Default"
  name: "Default"
current-context: "Default"
users:
- name: "Default"
  user:
    token: "QmFzaWMgT0VNeU0wWkZOa1EzTkVNNU5rRTBRVU16TkRBNlZWaExlREpuT0hCclVsQm9hRzF3TW1OR1V6Rm1ZV1o0VEdSU1VrZFhSVEpZTm5keGNsRkxPUT09"
```

如果无法连接请重试一遍

# 安装git客户端

```
yum -y install git
```

# 使用python 编码实现webhook

```
mkdir -p /data/tristan/k8s-ci && cd /data/tristan/k8s-ci
git clone http://gitlab.java.yibainetwork.com/Pass/k8s-ci.git
cd k8s-ci
cd backup

# 安装pip
yum -y install epel-release
yum -y install python-pip

pip install -r requirements.txt

python k8s-ci.py
nohup python k8s-ci.py &
```

访问:

http://112.74.32.223:7777



