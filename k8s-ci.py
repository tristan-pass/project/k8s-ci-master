#!/usr/bin/python
# -*- coding:utf8 -*-
import subprocess
import sys

from flask import Flask, render_template

reload(sys)
sys.setdefaultencoding("utf-8")

app = Flask(__name__, static_url_path='')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/deployed')
def index():
    data = {}
    return data


# @app.route('/update_image_id', methods=['POST'])
# def update_image_id(token=''):
#     render_template('index.html', token=token)
#     return redirect('/')


if __name__ == '__main__':
    app.run(
        host='127.0.0.1',
        # host='0.0.0.0',
        port=7777,
        debug=True
    )


def execute_command(command):
    """
    执行指令
    :param command:
    :return:
    """
    result = ""
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in p.stdout.readlines():
        try:
            result += line.decode('gbk')
        except Exception:
            try:
                result += line.decode('utf-8')
            except result:
                pass
    return result
