#!/usr/bin/python
# -*- coding:utf8 -*-
import subprocess
import sys

import flask

reload(sys)
sys.setdefaultencoding("utf-8")

app = flask.Flask(__name__)

index_temp = '''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>更新k8s的镜像id</title>
</head>
<body>
<form action="/update_image_id" method="post">
    token:  <input name="token"/> <br><br>
    please input the docker images id:  <input style="width: 1500px" name="docker_image_id"/> <br><br>
    <input type="submit" value="提交">
</form>
</body>
</html>
'''

my_token = "tristan"


@app.route('/')
def hello_world():
    return index_temp


@app.route('/update_image_id', methods=['POST'])
def update_image_id():
    message = "部署镜像中"
    token = flask.request.form['token']
    if not token or token != my_token:
        message = "访问必须提供有效token"
        return message
    docker_image_id = flask.request.form['docker_image_id']
    if not docker_image_id or docker_image_id == '':
        message = "镜像id不能为空"
    else:
        docker_image_id = (docker_image_id + "").strip()
        docker_image_info = (docker_image_id + "").split(":")
        if docker_image_info and len(docker_image_info) > 0 and docker_image_info[0]:
            docker_image_name = docker_image_info[0]
        docker_image_name = docker_image_name + ""
        deployment_name_index = (docker_image_name + "").rfind("/")
        if deployment_name_index and deployment_name_index >= 0 and deployment_name_index + 1 < len(docker_image_name):
            docker_deployment_name = (docker_image_name + "")[deployment_name_index + 1:]
            docker_deployment_name = (docker_deployment_name + "").replace("-", "", len(docker_deployment_name))
            print "docker_deployment_name: " + docker_deployment_name
        modify_image_version_command = "kubectl -n java set image deployment %s %s=%s" % (
            docker_deployment_name, docker_deployment_name, docker_image_id)
        print modify_image_version_command
        p = subprocess.Popen(modify_image_version_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in p.stdout.readlines():
            try:
                message += line.decode('gbk')
            except Exception:
                try:
                    message += line.decode('utf-8')
                except Exception:
                    pass
    return '''
            
            <br> %s <br> <br>
            ===================================
            ===================================
            <br>镜像id为"%s"<br> <br>
            ===================================
            ===================================
            <br> <a href="/"><- click to back home</a> <br> <br>
        ''' % (message, docker_image_id)


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=7777,
        debug=True
    )
